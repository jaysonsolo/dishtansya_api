<?php

use Faker\Factory as Faker;

class CreateUserFunctionTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_successfully_register_a_user()
    {
        $faker = Faker::create();

        $response = $this->post('/register', [
            'email' => $faker->email,
            'password' => 'test123',
        ]);

        $response->assertResponseStatus(201);
        $response->seeJsonStructure([
            "message"
        ]);
    }

    /**
     * @test
     */
    public function it_should_not_successfully_register_a_user()
    {
        $user = factory(\App\Models\User::class)->create();

        $response = $this->post('/register', [
            'email' => $user->email,
            'password' => 'test123',
        ]);

        $response->assertResponseStatus(400);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
    }
}
