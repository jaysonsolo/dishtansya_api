<?php

class AuthFunctionTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_generate_valid_token()
    {
        $user = factory(\App\Models\User::class)->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'test123',
        ]);

        $response->assertResponseStatus(201);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * @test
     */
    public function it_should_return_invalid_credentials()
    {
        $user = factory(\App\Models\User::class)->create();

        $response = $this->post('/login', [
            'email' => 'INVALID_' . $user->email,
            'password' => 'test123',
        ]);

        $response->assertResponseStatus(401);
        $response->seeJsonStructure([
            "error"
        ]);
    }
}
