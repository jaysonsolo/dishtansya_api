<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class OrderFunctionTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * @test
     */
    public function it_should_generate_valid_token()
    {
        $response = $this->post('/order', [
            'product_id' => 10,
            'quantity' => 1,
        ]);

        $response->assertResponseStatus(201);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    /**
     * @test
     */
    public function it_should_return_invalid_credentials()
    {
        $response = $this->post('/order', [
            'product_id' => 10,
            'quantity' => 100,
        ]);

        $response->assertResponseStatus(400);
        $response->seeJsonStructure([
            "error"
        ]);
    }
}
