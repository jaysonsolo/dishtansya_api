<?php

use Carbon\Carbon;

if (!function_exists('sanitize_url')) {
    /**
     * Sanitize URL. Add trailing slash.
     *
     */
    function sanitize_url($url, $slug = '')
    {
        return rtrim($url, '/') . '/' . ltrim($slug, '/');
    }
}

if (!function_exists('array_keys_exists')) {
    /**
     * compare if all keys exist
     */
    function array_keys_exists(array $keys, array $compare)
    {
        return !array_diff_key(array_flip($keys), $compare);
    }
}

if (!function_exists('check_production')) {
    /**
     * @return bool
     */
    function check_production()
    {
        if (config('app.env') === 'production' && config('app.debug') === false) {
            return true;
        }

        return false;
    }
}

if (!function_exists('request')) {
    /**
     * Get an instance of the current request or an input item from the request.
     *
     * @param  array|string  $key
     * @param  mixed   $default
     * @return \Illuminate\Http\Request|string|array
     */
    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('request');
        }

        if (is_array($key)) {
            return app('request')->only($key);
        }

        $value = app('request')->__get($key);

        return is_null($value) ? value($default) : $value;
    }
}

//https://laracasts.com/discuss/channels/lumen/lumen-get-route-parameters-value?page=1
if (!function_exists('route_parameter')) {
    /**
     * Get a given parameter from the route.
     *
     * @param $name
     * @param null $default
     * @return mixed
     */
    function route_parameter($name, $default = null)
    {
        $routeInfo = app('request')->route();

        return array_get($routeInfo[2], $name, $default);
    }
}

if (!function_exists('get_user_type_model')) {
    /**
     * Get model by user type
     *
     * @param $userType
     * @param $entity
     * @return mixed
     * @throws App\Services\EntityResolver\EntityNotFoundException
     * @throws \App\Services\UserType\UserTypeConfigException
     */
    function get_user_type_model($userType, $entity)
    {
        return App\Services\UserType\ModelFactory::make($userType, $entity);
    }
}

if (!function_exists('route_info')) {
    /**
     * Get a given parameter from the route.
     *
     * @param String $name
     * @param Null $default
     * @return String|Null
     */
    function route_info($name, $default = null)
    {
        $routeInfo = app('request')->route();

        return array_get($routeInfo[1], $name, $default);
    }
}

