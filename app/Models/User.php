<?php

namespace App\Models;

use App\BaseModels\BaseModel;

class User extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'login_failed_attempts',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
