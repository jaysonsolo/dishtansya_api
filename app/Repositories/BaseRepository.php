<?php

namespace App\Repositories;

use App\Repositories\Repository;

class BaseRepository extends Repository
{

    public function model()
    {
        //
    }

    /**
     * Create a new token.
     *
     * @param  $user
     * @return string
     */
    protected function jwtCreateToken($user)
    {

        $payload = [
            'iss' => "lumen-jwt",
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + config('jwt.lifetime') * 60
        ];

        return $this->jwt::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Decode token.
     *
     * @param  $token
     * @return string
     */
    protected function jwtDecodeToken($token)
    {
        return $this->jwt->decode($token, env('JWT_SECRET'), ['HS256']);
    }
}
