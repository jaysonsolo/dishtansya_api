<?php

namespace App\Repositories;

use Firebase\JWT\JWT;
use App\Exceptions\RepositoryInternalException;
use App\Exceptions\RepositoryBadRequestException;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;
use App\Contracts\Repository as RepositoryContract;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Arr;

/**
 * Class Repository
 *
 * @package App\Repositories
 */
abstract class Repository implements RepositoryContract
{
    private $inner_join = 'join';
    private $left_join = 'leftJoin';
    private $right_join = 'rightJoin';
    private $cross_join = 'crossJoin';

    /**
     * @var App
     */
    private $app;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var JWT
     */
    public $jwt;

    /**
     * Repository constructor.
     *
     * @throws RepositoryInternalException
     */
    public function __construct()
    {
        $this->app = new App();
        $this->jwt = new JWT();

        $this->makeModel();
    }

    /**
     * Assigning the model of the Repository
     *
     * @return mixed
     */
    abstract public function model();

    /**
     * Setting the model instance
     *
     * @param  $model
     * @return mixed
     * @throws RepositoryInternalException
     */
    public function setModel($model)
    {
        if (($this->model = $this->app->make($model)) instanceof Model) {
            return $this->model;
        }

        throw new RepositoryInternalException("{$model} must be an instance of Illuminate\\Database\\Eloquent\\Model");
    }

    /**
     * Making the model
     *
     * @return mixed
     * @throws RepositoryInternalException
     */
    public function makeModel()
    {
        return $this->setModel($this->model());
    }

    /**
     * Get all records in the DB
     *
     * @param  array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->model->get($columns);
    }

    /**
     * Find specific record by id
     *
     * $this->find(1) -> will return the record with all of its columns
     * $this->find(1,['id']) -> will return the record with the column id
     * $this->find(1,['id'], true) -> will return the record with all of its columns except the id
     *
     * @param  $id
     * @param  array $columns
     * @param  boolean $exclude
     * @return mixed
     */
    public function find($id, $columns = array('*'), $exclude = false)
    {
        if ($exclude) {
            return $this->model->exclude($columns)->where($this->model->getKeyName(), $id)->first();
        }

        return $this->model->select($columns)->where($this->model->getKeyName(), $id)->first();
    }

    /**
     * Find first occurrence of record by specific column and value
     *
     * $this->findBy('id',1) -> will return the record with all of its columns
     * $this->findBy('id',1,['id']) -> will return the record with the column id
     * $this->findBy('id',1,['id'], true) -> will return the record with all of its columns except the id
     *
     * @param  $attribute
     * @param  $value
     * @param  array $columns
     * @param  boolean $exclude
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*'), $exclude = false)
    {
        if ($exclude) {
            return $this->model->exclude($columns)->where($attribute, '=', $value)->first();
        }

        return $this->model->select($columns)->where($attribute, '=', $value)->first();
    }

    /**
     * Find last occurrence of record by specific column and value
     *
     * $this->findLastBy('id',1) -> will return the record with all of its columns
     * $this->findLastBy('id',1,['id']) -> will return the record with the column id
     * $this->findLastBy('id',1,['id'], true) -> will return the record with all of its columns except the id
     *
     * @param  $attribute
     * @param  $value
     * @param  array $columns
     * @return mixed
     */
    public function findLastBy($attribute, $value, $columns = array('*'), $exclude = false)
    {
        if ($exclude) {
            return $this->model->exclude($columns)->where($attribute, '=', $value)->latest()->first();
        }

        return $this->model->select($columns)->where($attribute, '=', $value)->latest()->first();
    }

    /**
     * Find all records by specific column and value
     *
     * $this->findAllBy('id',1) -> will return all records with all of its columns
     * $this->findAllBy('id',1,['id']) -> will return all records with the column id
     * $this->findAllBy('id',1,['id'], true) -> will return all records with all of its columns except the id
     *
     * @param  $attribute
     * @param  $value
     * @param  array $columns
     * @param  boolean $exclude
     * @return mixed
     */
    public function findAllBy(
        $attribute,
        $value,
        $columns = array('*'),
        $exclude = false,
        $order_col = 'created_at',
        $order_setting = 'asc'
    ) {
        if ($exclude) {
            return $this->model->exclude($columns)
                ->where($attribute, '=', $value)
                ->orderBy($order_col, $order_setting)
                ->get();
        }

        return $this->model->select($columns)
            ->where($attribute, '=', $value)
            ->orderBy($order_col, $order_setting)
            ->get();
    }

    /**
     * Find a collection of models by the given query conditions.
     *
     * $this->findWhere(['id' => 1]) -> will return all records with all of its columns
     * $this->findWhere(['id' => 1],['id']) -> will return all records with the column id
     * $this->findWhere(['id' => 1],['id'], ['permissions' => 'permission_id']) ->
     * will return all records with all of its columns except the id
     *
     * $this->findWhere(['id' => 1],['id'], ['permissions' => 'permission_id'],['id']) ->
     * will return all records with the column id and grouped by id
     *
     * $this->findWhere(['id' => 1],['id'], ['permissions' => 'permission_id'],['id'],['role']) ->
     * will return all records with the column id and grouped by id with role relationship
     *
     * $this->findWhere(['id' => 1],['id'], ['permissions' => 'permission_id'],true) ->
     * will return all records with the column id but the where conditions will have the OR operator instead of AND
     *
     * $this->findWhere([['id',1],['name', '=', 'admin']],['id'], [['create_roles_table.sql', 'role_id'],
     * ['role_has_permissions', 'role_id'],['permissions', 'permission_id']]) ->
     * will return all records eligible with the query conditions
     *
     * $this->findWhere([['id',1],['name', '=', 'admin']],['id'], [['create_roles_table.sql', 'INNER_JOIN', 'role_id']
     * ,['role_has_permissions', 'LEFT_JOIN', 'role_id'],['permissions', 'RIGHT_JOIN', 'permission_id']]) ->
     * will return all records eligible with the query conditions
     *
     * @param array $where
     * @param array $columns
     * @param array $group
     * @param array $join
     * @param bool $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     * @throws RepositoryInternalException
     */
    public function findWhere($where, $columns = ['*'], $join = [], $group = [], $with = [], $or = false)
    {
        $model = $this->model->select($columns);

        if (count($join) > 0) {
            $model = $this->buildJoin($join, $model);
        }

        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = (!$or)
                    ? $model->where($value)
                    : $model->orWhere($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = (!$or)
                        ? $model->where($field, $operator, $search)
                        : $model->orWhere($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = (!$or)
                        ? $model->where($field, '=', $search)
                        : $model->orWhere($field, '=', $search);
                }
            } else {
                $model = (!$or)
                    ? $model->where($field, '=', $value)
                    : $model->orWhere($field, '=', $value);
            }
        }

        if (count($group) > 0) {
            $model = $model->groupBy($group);
        }

        if (count($with) > 0) {
            $model = $model->with($with);
        }

        return $model->get();
    }

    /**
     * Build the Join Queries if the join parameter in findWhere method is not empty
     *
     * @param  $join
     * @param  $model
     * @return mixed
     * @throws RepositoryInternalException
     */
    public function buildJoin($join, $model)
    {
        $valid_joins = [
            'INNER_JOIN',
            'RIGHT_JOIN',
            'LEFT_JOIN',
            'CROSS_JOIN'
        ];

        $count = 0;
        foreach ($join as $table_key => $value) {
            if ($value instanceof \Closure) {
                $model = $model->join($value);
            } elseif (is_array($value)) {
                if (Arr::isAssoc($value)) {
                    foreach ($value as $key => $item) {
                        $model = $model->join($key, $key . '.' . $item, $this->getTable() . '.' . $item);
                    }
                } elseif (count($value) === 4) {
                    list($table, $join, $connection, $join_con) = $value;

                    if (in_array($join, $valid_joins)) {
                        $join = strtolower($join);
                        $join = $this->{$join};
                        $model = $model->{$join}($table, $table . '.' . $connection, $join_con);
                    } else {
                        throw new RepositoryInternalException("The parameter join '{$join}' is invalid.");
                    }
                } elseif (count($value) === 3) {
                    list($table, $join, $connection) = $value;

                    if (in_array($join, $valid_joins)) {
                        if ($count === 0) {
                            $model = $model->{$this . "::{$join}"}(
                                $table,
                                $table . '.' . $connection,
                                $this->getTable() . '.' . $connection
                            );
                        } else {
                            if (count($join[$count - 1]) === 3) {
                                list($con_table, $con_join, $con_column) = $join[$count - 1];
                            } else {
                                list($con_table, $con_column) = $join[$count - 1];
                            }

                            $model = $model->{$this . "::{$join}"}(
                                $table,
                                $table . '.' . $connection,
                                $con_table . '.' . $con_column
                            );
                        }
                    } else {
                        throw new RepositoryInternalException("The parameter join '{$join}' is invalid.");
                    }
                } elseif (count($value) === 2) {
                    list($table, $connection) = $value;

                    if ($count === 0) {
                        $model = $model->join(
                            $table,
                            $table . '.' . $connection,
                            $this->getTable() . '.' . $connection
                        );
                    } else {
                        if (count($join[$count - 1]) === 3) {
                            list($con_table, $con_join, $con_column) = $join[$count - 1];
                        } else {
                            list($con_table, $con_column) = $join[$count - 1];
                        }

                        $model->join($table, $table . '.' . $connection, $con_table . '.' . $con_column);
                    }
                }
            } else {
                $model->join($table_key, $table_key . '.' . $value, $this->getTable() . '.' . $value);
            }

            $count++;
        }

        return $model;
    }

    /**
     * Get the model table name
     *
     * @return mixed
     */
    public function getTable()
    {
        return $this->model->getTable();
    }

    /**
     * Create new record for the model
     *
     * @param  array $params
     * @return bool|mixed
     */
    public function createNew(array $params)
    {
        if ($this->model->create($params)) {
            return true;
        }

        return false;
    }

    /**
     * Update record by id for the model
     *
     * @param  int $id
     * @param  array $params
     * @return bool|mixed
     * @throws RepositoryBadRequestException
     */
    public function update(int $id, array $params)
    {
        if ($model = $this->find($id)) {
            if ($model->update($params)) {
                return true;
            }

            return false;
        }

        throw new RepositoryBadRequestException("Record not found!");
    }

    /**
     * Delete record by id for the model
     *
     * @param  int $id
     * @return bool|mixed
     * @throws RepositoryBadRequestException
     */
    public function delete(int $id)
    {
        if ($model = $this->find($id)) {
            if ($model->delete()) {
                return true;
            }
        }

        throw new RepositoryBadRequestException("Record not found!");
    }
}
