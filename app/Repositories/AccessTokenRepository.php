<?php

namespace App\Repositories;

use App\Models\v1\AccessToken;

class AccessTokenRepository extends BaseRepository
{
    public function model()
    {
        return AccessToken::class;
    }
}
