<?php

namespace App\Repositories;

use App\Models\AccessToken;
use App\Models\User;
use Exception;
use App\Models\v1\RoleCredential;
use Firebase\JWT\ExpiredException;

class JwtRepository extends BaseRepository
{
    protected $accessToken;
    protected $userIdColumnName;

    public function __construct()
    {
        parent::__construct();

        $this->accessToken = AccessToken::class;
        $this->userIdColumnName = 'user_id';

    }

    public function model()
    {
        return User::class;
    }

    /**
     * Validate token
     *
     * @param  $token
     * @return mixed
     */
    public function validateToken($token)
    {
        try {
            if ($access_token = $this->accessToken::where('token', $token)->first()) {
                if ($access_token->token === $token) {
                    return true;
                }
            }

            return false;
        } catch (Exception $e) {
            return false;
        }
    }
}
