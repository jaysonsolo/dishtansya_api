<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Hash;

class OrderRepository extends BaseRepository
{
    /**
     * Set Model for the Repository
     *
     * @return mixed|string|void
     */
    public function model()
    {
        return Order::class;
    }

    /**
     * @param $userRequest
     * @return array
     * @throws RepositoryInternalException
     * @throws ValidationResponseException
     */
    public function createOrder($userRequest)
    {
        $product = Product::whereId($userRequest->product_id)->first();
        
        if ($userRequest->quantity > $product->available_stock) {
            return false;
        }

        $quantity = $product->available_stock - $userRequest->quantity;

        Product::whereId($userRequest->product_id)->update(['available_stock' => $quantity]);

        $createOrder = $this->model->create([
            'product_id' => $userRequest->product_id, 
            'quantity' => $userRequest->quantity
        ]);

        if ($createOrder) {
            return true;
        }
    }
}
