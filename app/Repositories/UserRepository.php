<?php

namespace App\Repositories;

use App\Jobs\RegisterEmailJob;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Queue;

class UserRepository extends BaseRepository
{
    /**
     * Set Model for the Repository
     *
     * @return mixed|string|void
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @param $userRequest
     * @return array
     */
    public function createUser($userRequest)
    {
        $createUser = $this->model->create(['email' => $userRequest->email, 'password' => Hash::make($userRequest->password)]);

        $emailJob = (new RegisterEmailJob($createUser));
        Queue::push($emailJob);

        // dispatch($emailJob);

        if ($createUser) {
            return ['email' => $userRequest->email];
        }
    }
}
