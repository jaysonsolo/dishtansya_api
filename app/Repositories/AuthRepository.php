<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\User;
use App\Models\AccessToken;
use Illuminate\Support\Facades\Hash;

class AuthRepository extends BaseRepository
{
    const MAX_RETRY_ATTEMPTS = 5;
    const ACCOUNT_LOCKED_TIME = 5;

    public function model()
    {
        return User::class;
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param $credentials
     * @return mixed
     */
    public function token($credentials)
    {
        $user = $this->model->where('email', $credentials['email'])->first();
        $token = false;

        if ($user) {
            if (Hash::check($credentials['password'], $user->password)) {
                $token = $this->generateToken($user);
            }
        }

        return $token;
    }

    /**
     * Generates token using user_id.
     *
     * @param $user
     * @return mixed
     */
    private function generateToken($user)
    {
        if (!empty($user)) {
            $this->removeExpiredAndUnusedTokens($user->id);
            $token = $this->jwtCreateToken($user);
            $expiresAt = Carbon::createFromTimestamp($this->jwtDecodeToken($token)->exp)->format('Y-m-d h:i:s');

            // Create new access token
            $accessToken = new AccessToken();
            $accessToken->user_id = $user->id;
            $accessToken->token = $token;
            $accessToken->expires_at = $expiresAt;
            $accessToken->save();

            if ($accessToken) {
                $this->failedAttemptsCount($user->email, 0);

                return [
                    'acess_token' => $token,
                    'expires_at' => $expiresAt
                ];
            }
        }

        return false;
    }

    /**
     * Remove expired or unused tokens.
     *
     * @param  $userID
     * @return mixed
     */
    private function removeExpiredAndUnusedTokens($userID)
    {
        AccessToken::where('expires_at', '<', Carbon::now())->each(
            function ($item) {
                $item->delete();
            }
        );

        if ($access_token = AccessToken::where('user_id', $userID)) {
            $access_token->delete();
        }

        return true;
    }

    /**
     * Failed attempts.
     *
     * @param $user
     * @return mixed
     */
    public function lockAccount($request)
    {
        $user = User::whereEmail($request['email'])->first();
        
        if ($user) {
            $attempts = $user->login_failed_attempts;

            if ($attempts >= self::MAX_RETRY_ATTEMPTS && Carbon::now() > Carbon::parse($user->updated_at)->addMinutes(self::ACCOUNT_LOCKED_TIME)) {
                $this->failedAttemptsCount($request['email'], 0);
                return false; //Account Unlocked
            } else {
                $attempts += 1;
                $this->failedAttemptsCount($request['email'], $attempts);

                if ($attempts >= self::MAX_RETRY_ATTEMPTS) {
                    return true; //Account Locked
                }
            }
        }

        return false;
    }

    /**
     * Failed attempts.
     *
     * @param $user
     * @return mixed
     */
    public function unlockAccount($request)
    {
        $user = User::whereEmail($request['email'])->first();
        
        if ($user) {
            if (Carbon::now()->addMinutes(5) > $user->updated_at) {
                $this->failedAttemptsCount($request['email'], 0);
                return true; //Account Unlocked
            }
        }

        return false;
    }

    /**
     * Update Failed attempts.
     *
     * @param $email
     * @param $count
     * @return mixed
     */
    private function failedAttemptsCount($email, $count)
    {
        User::whereEmail($email)->update(['login_failed_attempts' => $count]);
    }
}
