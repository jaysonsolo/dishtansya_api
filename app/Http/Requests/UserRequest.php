<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;
use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        $return = [
            'email' => 'required|unique:users|email',
            'password' => "required|min:6",
        ];

        return $return;
    }

    protected function messages()
    {
        return [
           'email.unique' => '“Email already taken.'
        ];
    }
}
