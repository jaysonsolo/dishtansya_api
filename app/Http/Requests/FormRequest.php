<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use App\Exceptions\ValidationResponseException;
use Illuminate\Validation\UnauthorizedException;

abstract class FormRequest extends Request
{
    const GET_METHOD = 'GET';
    const POST_METHOD = 'POST';
    const PUT_METHOD = 'PUT';
    const PATCH_METHOD = 'PATCH';
    const DELETE_METHOD = 'DELETE';

    public static $USER_DATA;
    public static $APP_TYPE;

    public $errors;

    public function validate()
    {
        if (false === $this->authorize()) {
            throw new UnauthorizedException();
        }

        $validator = app('validator')->make($this->all(), $this->rules(), $this->messages());

        if ($validator->fails()) {
            throw new ValidationResponseException(
                'Validation error!',
                $validator->errors()
            );
        }
    }

    protected function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    protected function messages()
    {
        return [
            //
        ];
    }

    public function attributes()
    {
        return [
            //
        ];
    }

    public function userData()
    {
        return self::$USER_DATA;
    }

    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
