<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Requests\FormRequest;
use App\Repositories\JwtRepository;

class JwtMiddleware
{
    private $jwtRepository;

    /**
     * UserController constructor.
     *
     * @param $jwtRepository
     */
    public function __construct(JwtRepository $jwtRepository)
    {
        $this->jwtRepository = $jwtRepository;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('Authorization');

        if ($token) {
            $token = explode('Bearer ', $token);

            $response = $this->jwtRepository->validateToken($token[1]);
            if ($response) {
                return $next($request);
            }

            return response()->json(
                [
                    'error' => 'Invalid token.'
                ],
                401
            );
        }

        // Unauthorized response if token not there
        return response()->json(
            [
                'error' => 'Token not provided.'
            ],
            400
        );
    }
}
