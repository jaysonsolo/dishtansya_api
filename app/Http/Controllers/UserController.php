<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UserController extends BaseController
{
    public function create(UserRequest $userRequest)
    {
        if ($response = (new UserRepository())->createUser($userRequest)) {
            $this->flagAction = true;
            return $this->sendResponseOk($response, 'User successfully registered');
        }

        return $this->sendBadRequest([], 'User failed to register');
    }
}
