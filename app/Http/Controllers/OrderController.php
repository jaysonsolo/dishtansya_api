<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    public function create(Request $userRequest)
    {
        if ((new OrderRepository())->createOrder($userRequest)) {
            $this->flagAction = true;
            return $this->sendResponseOk([], 'You have successfully ordered this product.');
        }

        return $this->sendBadRequest([], 'Failed to order this product due to unavailability of the stock');
    }
}
