<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Repositories\AuthRepository;

class AuthController extends BaseController
{
    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param $authRequest
     * @return mixed
     */
    public function token(Request $authRequest)
    {
        $response = (new AuthRepository())->token($authRequest->all());

        if ($response) {
            $this->flagAction = true;
            return $this->sendResponseOk($response, 'Token generated');
        }

        if ((new AuthRepository)->lockAccount($authRequest->all())) {
            return $this->sendUnauthorized([], 'Account Locked!');
        }

        return $this->sendUnauthorized([], 'Invalid credentials');
    }
}
