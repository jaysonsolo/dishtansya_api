<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ProductInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Install dummy products.";

    /**
     * Execute the console command.
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle()
    {
        $this->info('Seeding...');
        $this->call('db:seed');
    }
}
