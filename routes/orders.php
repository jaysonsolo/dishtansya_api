<?php
$router->group(['middleware' => ['jwt.auth']], function () use ($router) {
    $router->post('/order', [
        'uses' => 'OrderController@create',
    ]);
});

