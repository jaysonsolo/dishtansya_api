<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <=20; $i++) {
            \App\Models\Product::create([
                'name' => 'Product' . $i,
                'available_stock' => 10,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
